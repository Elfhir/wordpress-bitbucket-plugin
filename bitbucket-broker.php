<?php

require ('lib/tmhOAuth.php');
require ('lib/tmhUtilities.php');

class BitbucketBroker {
	
	public static $apiBase = "https://api.bitbucket.org/1.0/";
	public static $urlBase = "https://bitbucket.org/";
	var $username, $repository;
	var $key, $secret, $links;
	var $apiUrl, $publicUrl, $filter;
	var $items, $resolvedItems;
 
	function __construct($username, $repository, $type, $filter) {
		$this->username = str_replace(' ', '-', $username);
		$this->repository = strtolower(str_replace(' ', '-', $repository));
		$this->type = $type;
		$this->filter = array_filter($filter);

    $options = get_option('bbi_options');
		$this->key = $options['key'];
		$this->secret = $options['secret'];
		$this->links = $options['links'];

    // Build filte string
    $array = array();
    foreach ($filter as $key => $value) {
        if ($value) {
          $array[] = $key . '=' . $value;
        }
    }
    $filter = count($array) ? '?' . implode("&", $array) : '';

    // Generate url's
    $this->api['issues'] = self::$apiBase . 'repositories/' . $this->username . '/' . $this->repository . '/issues';
    $this->public['issues'] = self::$urlBase . $this->username . '/' . $this->repository . '/issues' . $filter;
    $this->api['changesets'] = self::$apiBase . 'repositories/' . $this->username . '/' . $this->repository . '/changesets';
	}
	
	function request($url) {
		$tmhOAuth = new tmhOAuth(array(
			'host' => 'api.bitbucket.org',
  		'consumer_key' => $this->key,
  		'consumer_secret' => $this->secret,
  	));

		// retry the request up to 5 times
		$i = 0;
		do {
			$tmhOAuth->request('GET', $url, $this->filter, false);
			if ($tmhOAuth->response['code'] != 200) {
				$tmhOAuth->request('GET', $url, $this->filter);
			}
		} while($tmhOAuth->response['code'] != 200 && $i++ < 5);

		if ($tmhOAuth->response['code'] != 200) {
			return;
		}
		return json_decode($tmhOAuth->response['response']);
	}

	function issues() {
		// Return issues staight away if available
		if ($this->items)
			return $this->items;
		// Fetch data
		$json = $this->request($this->api['issues']);
		if ($json) {
			// print_r($json);
			$this->items = array();
			$this->resolvedItems = 0;
			foreach ($json->issues as $issue) {
				$issue =  new BitbucketIssue($this, $issue);
				$this->resolvedItems += $issue->status == "resolved" ? 1 : 0;
				$this->items[] = $issue;
			}
			return $this->items;
		}
		else {
			$this->items = array();
			$this->resolvedItems = 0;
		}
		return 401;
	}

	function changesets() {
		// Return issues staight away if available
		if ($this->items)
			return $this->items;
		// Fetch data
		$json = $this->request($this->api['changesets']);
		if ($json) {
			// print_r($json);
			$this->items = array();
			foreach (array_reverse($json->changesets) as $changeset) {
				$changeset =  new BitbucketChangeset($this, $changeset);
				$this->items[] = $changeset;
			}
			return $this->items;
		}
		else {
			$this->items = array();
		}
		return 401;
	}
}

class BitbucketIssue {

		var $id, $title, $description, $created, $updated, $kind, $priority, $status, $assignee, $version, $milestone, $component;
		var $url, $search, $replace;

		function __construct($broker, $issue) {
			$this->id = $issue->local_id;
			$this->title = $issue->title;
			$this->description = $issue->content;
			$this->created = date_format(date_create($issue->utc_created_on) ,'d-m-Y');
			$this->updated = date_format(date_create($issue->utc_last_updated) ,'d-m-Y');
			$this->kind = $issue->metadata->kind;
			$this->priority = $issue->priority;
			$this->status = $issue->status;
			$this->assignee = $issue->responsible ? $issue->responsible->username : "unassigned";
			$this->version = $issue->metadata->version;
			$this->milestone = $issue->metadata->milestone;
			$this->component = $issue->metadata->component;

			$this->url = ($broker->links == 1 || ($broker->links != 2 && $broker->type == 'public')) ? BitbucketBroker::$urlBase . $broker->username . '/' . $broker->repository . '/issue/' . $this->id : '';
			$this->search = array("{%title}", "{%description}", "{%created}", "{%updated}", "{%kind}", "{%priority}", "{%status}", "{%assignee}", "{%version}", "{%milestone}", "{%component}");
			$this->replace = array($this->title, $this->description, $this->created, $this->updated, ucwords($this->kind), $this->priority, $this->status, $this->assignee, $this->version, $this->milestone, $this->component);
		}
}

class BitbucketChangeset {

	var $node, $raw_node, $shortnode, $author, $timestamp, $branch, $message;
	var $url, $search, $replace;

	function __construct($broker, $changeset) {
		$this->node = $changeset->node;
		$this->raw_node = $changeset->raw_node;
		$this->commit = substr($changeset->node, 0, 7);
		$this->author = $changeset->author;
		$this->date = date_format(date_create($changeset->utctimestamp) ,'d-m-Y');
		$this->branch = trim($changeset->branch);
		$this->message = $changeset->message;

		$this->url = ($broker->links == 1 || ($broker->links != 2 && $broker->type == 'public')) ? BitbucketBroker::$urlBase . $broker->username . '/' . $broker->repository . '/commits/' . $this->raw_node : '';
		$this->search = array("{%node}", "{%raw_node}", "{%commit}", "{%author}", "{%date}", "{%branch}", "{%message}");
		$this->replace = array($this->node, $this->raw_node, $this->commit, $this->author, $this->date, $this->branch, $this->message);
	}
}

?>